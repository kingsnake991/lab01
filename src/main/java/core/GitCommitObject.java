package core;

import java.io.IOException;

public class GitCommitObject extends GitBlobObject{

    private String treeHash = "";
    private String parentHash = "";
    private String authorName = "";
    private String masterCommitHash = "";

    public GitCommitObject(String repoPath, String masterCommitHash) throws IOException {
        super(repoPath, masterCommitHash);

        // Get the contents of the file associated with this hash
        String[] content = this.getContent().split("\n");

        //Save masterHash
        this.masterCommitHash = masterCommitHash;

        //Save treeHash
        this.treeHash = content[0].split(" ")[1];

        //Save parentHash
        this.parentHash = content[1].split(" ")[1];

        String[] result = content[2].split(" ");

        //Save authorName
        this.authorName = result[1] + " " + result[2] + " " + result[3];
    }

    /**
     * Get hash
     * @return
     */
    public String getHash() {
        return this.masterCommitHash;
    }

    /**
     * Get tree hash
     * @return
     */
    public String getTreeHash() {
        return this.treeHash;
    }

    /**
     * Get parent hash
     * @return
     */
    public String getParentHash() {
        return this.parentHash;
    }

    /**
     * Get author
     * @return
     */
    public String getAuthor() {
        return this.authorName;
    }
}
